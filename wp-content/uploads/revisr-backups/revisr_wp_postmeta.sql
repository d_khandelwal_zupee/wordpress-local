DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
LOCK TABLES `wp_postmeta` WRITE;
INSERT INTO `wp_postmeta` VALUES ('1','2','_wp_page_template','default'), ('2','3','_wp_page_template','default'), ('4','3','_edit_lock','1640605793:1'), ('7','8','_elementor_edit_mode','builder'), ('8','8','_elementor_template_type','kit'), ('27','28','_edit_lock','1641287146:1'), ('28','28','_edit_last','1'), ('31','37','_edit_lock','1644501731:1'), ('34','37','_wp_trash_meta_status','publish'), ('35','37','_wp_trash_meta_time','1644501996'), ('36','37','_wp_desired_post_slug','test-post'), ('37','39','_edit_lock','1644502415:1');
UNLOCK TABLES;
