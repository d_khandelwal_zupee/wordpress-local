DROP TABLE IF EXISTS `wp_vp_id`;
CREATE TABLE `wp_vp_id` (
  `vp_id` binary(16) NOT NULL,
  `table` varchar(64) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`vp_id`),
  UNIQUE KEY `table_id` (`table`,`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `wp_vp_id` WRITE;
INSERT INTO `wp_vp_id` VALUES ('r�< �QA��IZ\"�Xj�','comments','1'), ('�D����H��>b6Uw�7','postmeta','1'), ('g@���G/���/�J�','postmeta','2'), ('�!@�3�L��Cp�&�e�','postmeta','7'), ('\r�9��Ow��	�.g1�','postmeta','8'), ('t�b��I����E��6�','postmeta','34'), ('�nTۓ�J��EQ&�I��','postmeta','35'), ('﾿Ђ�B��}<o�','postmeta','36'), ('��*��F�(\'7�w/O','posts','1'), ('���K�J�����W��','posts','2'), ('as��`C���qA��','posts','3'), ('�J��{CH��q喹p�','posts','8'), ('�e-��AJ����~�P','posts','28'), ('Ϡ��0<Bu���+��(�','posts','37'), ('�YOT[J���q�)�h<','posts','39'), ('�g\\ļF0��E�>�~�','terms','1'), ('��3_�ES����}^�','term_taxonomy','1'), ('\ZKc�0�D���|\"\r(�','usermeta','1'), ('㤮�qAF��>�ظY�','usermeta','2'), ('����L⬋Ǫ#:S','usermeta','3'), ('��#���M���q H�','usermeta','4'), ('�A�x�@f���\n�!J�','usermeta','5'), ('��fJKRKo�91�R�޾','usermeta','6'), ('�su�ιKX�ֿ`��o�','usermeta','7'), ('.s��2�Le�UsH��~�','usermeta','8'), (';El]{�Ll��=\rsƙ�','usermeta','9'), ('�^�3�M2�V�`\r�Ә','usermeta','10'), ('��ͥ�w@*���o]GO','usermeta','11'), ('�����M3�v�L�W��','usermeta','12'), ('�kV�|D[��Z AP�','usermeta','13'), ('\0�1��wF[���˽�s','usermeta','14'), ('��s��@FǖHx�)]\r','usermeta','15'), ('2vQ3�E5�P�w��Z','usermeta','17'), ('��{�By��ņ�u�)','usermeta','18'), ('�4,��DH��熃��8','usermeta','20'), ('|��ZȾN��E`��1','users','1');
UNLOCK TABLES;
