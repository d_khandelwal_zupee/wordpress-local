DROP TABLE IF EXISTS `wp_jenkins_activity_log`;
CREATE TABLE `wp_jenkins_activity_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(60) NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL DEFAULT 'updated',
  `activity_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
LOCK TABLES `wp_jenkins_activity_log` WRITE;
INSERT INTO `wp_jenkins_activity_log` VALUES ('1','deepak','pushed to production','2022-03-02 11:05:02'), ('2','deepak','pushed to production','2022-03-02 11:15:18'), ('3','deepak','pushed to production','2022-03-02 14:04:07');
UNLOCK TABLES;
